package pl.edu.pwsz.cinema.repertoire.dto;

import lombok.Builder;
import lombok.Getter;
import pl.edu.pwsz.cinema.repertoire.entity.Movie;

@Getter
@Builder
public class MovieDTO {
    private final int id;
    private final String title;
    private final String description;
    private final String cover;
    private final int rating;
    private final int price;

    public static MovieDTO from(Movie movie) {
        return MovieDTO.builder()
                .id(movie.getId())
                .title(movie.getTitle())
                .description(movie.getDescription())
                .rating(movie.getRating())
                .cover(movie.getCover())
                .price(movie.getPrice())
                .build();
    }
}
