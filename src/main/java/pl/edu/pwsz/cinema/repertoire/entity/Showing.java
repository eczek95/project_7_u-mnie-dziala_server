package pl.edu.pwsz.cinema.repertoire.entity;

import lombok.Getter;
import lombok.Setter;
import pl.edu.pwsz.cinema.commerce.entity.Ticket;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
public class Showing {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private LocalDateTime playingDate;

    @ManyToOne
    private Movie movie;
    @ManyToOne
    private Room room;
    @OneToMany(mappedBy = "showing")
    private List<Ticket> tickets;

    @Transient
    private Integer ticketsCount = null;
    @Transient
    private Integer vipTicketsCount = null;

    @Transient
    public int countTickets() {
        if (ticketsCount == null) {
            calculateTickets();
        }

        return ticketsCount;
    }

    @Transient
    public int countVipTickets() {
        if (vipTicketsCount == null) {
            calculateTickets();
        }

        return vipTicketsCount;
    }

    @Transient
    private void calculateTickets() {
        ticketsCount = 0;
        vipTicketsCount = 0;

        for (Ticket ticket : tickets) {
            if (ticket.isVip()) {
                vipTicketsCount++;
            } else {
                ticketsCount++;
            }
        }
    }
}
