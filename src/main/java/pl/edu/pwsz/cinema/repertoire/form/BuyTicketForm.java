package pl.edu.pwsz.cinema.repertoire.form;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuyTicketForm {
    private String firstName;
    private String lastName;
    private int showing;
    private boolean vip;
}
