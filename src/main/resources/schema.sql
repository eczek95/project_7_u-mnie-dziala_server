CREATE TABLE movie (
    id          INT           NOT NULL AUTO_INCREMENT,
    title       VARCHAR(255)  NOT NULL,
    description VARCHAR(1000) NOT NULL,
    cover       VARCHAR(255)  NOT NULL,
    rating      INT           NOT NULL,
    price       INT           NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE room (
    id        INT NOT NULL AUTO_INCREMENT,
    seats     INT NOT NULL,
    vip_seats INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE showing (
    id           INT      NOT NULL AUTO_INCREMENT,
    movie_id     INT      NOT NULL,
    room_id      INT      NOT NULL,
    playing_date DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT SHOWING_MOVIE_FK FOREIGN KEY (movie_id) REFERENCES movie(id),
    CONSTRAINT SHOWING_ROOM_FK FOREIGN KEY (room_id) REFERENCES room(id)
);

CREATE TABLE ticket (
    id            INT          NOT NULL AUTO_INCREMENT,
    showing_id    INT          NOT NULL,
    creation_date DATE         NOT NULL,
    first_name    VARCHAR(255) NOT NULL,
    last_name     VARCHAR(255) NOT NULL,
    is_vip        BOOLEAN      NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT TICKET_SHOWING_FK FOREIGN KEY (showing_id) REFERENCES showing(id)
);

CREATE INDEX MOVIE_TITLE_IDX ON movie(title);
