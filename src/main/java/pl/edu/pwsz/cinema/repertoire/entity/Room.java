package pl.edu.pwsz.cinema.repertoire.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Room {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int seats;
    private int vipSeats;
}
