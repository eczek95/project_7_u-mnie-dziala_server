package pl.edu.pwsz.cinema.repertoire.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.edu.pwsz.cinema.repertoire.entity.Showing;
import pl.edu.pwsz.cinema.repertoire.repository.ShowingRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ShowingService {

    private final ShowingRepository showingRepository;

    public List<Showing> getShowings() {
        return showingRepository.findAll();
    }
}
