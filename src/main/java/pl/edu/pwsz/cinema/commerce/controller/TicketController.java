package pl.edu.pwsz.cinema.commerce.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pwsz.cinema.commerce.entity.Ticket;
import pl.edu.pwsz.cinema.commerce.repository.TicketRepository;
import pl.edu.pwsz.cinema.repertoire.entity.Showing;
import pl.edu.pwsz.cinema.repertoire.form.BuyTicketForm;
import pl.edu.pwsz.cinema.repertoire.repository.ShowingRepository;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;


@RestController
@RequiredArgsConstructor
public class TicketController {

    private final ShowingRepository showingRepository;
    private final TicketRepository ticketRepository;

    @CrossOrigin(origins = "*")
    @PostMapping(path = "/ticket", produces = "application/json; charset=UTF-8")
    public Map<String, Boolean> createTicket(@RequestBody BuyTicketForm form) {
        Showing showing = showingRepository.getById(form.getShowing());

        if (form.isVip()) {
            if (showing.countVipTickets() < showing.getRoom().getVipSeats()) {
                registerTicket(showing, form);

                return Collections.singletonMap("success", true);
            }
        } else {
            if (showing.countTickets() < showing.getRoom().getSeats()) {
                registerTicket(showing, form);

                return Collections.singletonMap("success", true);
            }
        }

        return Collections.singletonMap("success", false);
    }

    private void registerTicket(Showing showing, BuyTicketForm form) {
        Ticket ticket = new Ticket();
        ticket.setShowing(showing);
        ticket.setFirstName(form.getFirstName());
        ticket.setLastName(form.getLastName());
        ticket.setVip(form.isVip());
        ticket.setCreationDate(LocalDateTime.now());

        ticketRepository.save(ticket);
    }
}
