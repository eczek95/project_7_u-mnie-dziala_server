package pl.edu.pwsz.cinema.repertoire.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.edu.pwsz.cinema.repertoire.entity.Movie;
import pl.edu.pwsz.cinema.repertoire.repository.MovieRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MovieService {

    private final MovieRepository movieRepository;

    public List<Movie> getMovies() {
        return movieRepository.findAll();
    }
}
