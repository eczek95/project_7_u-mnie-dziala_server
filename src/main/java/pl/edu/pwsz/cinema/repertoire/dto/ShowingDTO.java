package pl.edu.pwsz.cinema.repertoire.dto;

import lombok.Builder;
import lombok.Getter;
import pl.edu.pwsz.cinema.repertoire.entity.Movie;
import pl.edu.pwsz.cinema.repertoire.entity.Showing;

import java.time.LocalDateTime;

@Getter
@Builder
public class ShowingDTO {
    private final int id;
    private final LocalDateTime playingDate;
    private final int seats;
    private final int vipSeats;
    private final int tickets;
    private final int vipTickets;
    private final int movie;

    public static ShowingDTO from(Showing showing) {
        return ShowingDTO.builder()
                .id(showing.getId())
                .playingDate(showing.getPlayingDate())
                .seats(showing.getRoom().getSeats())
                .vipSeats(showing.getRoom().getVipSeats())
                .tickets(showing.countTickets())
                .vipTickets(showing.getVipTicketsCount())
                .movie(showing.getMovie().getId())
                .build();
    }
}
