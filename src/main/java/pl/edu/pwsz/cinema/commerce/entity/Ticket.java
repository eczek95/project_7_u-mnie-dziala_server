package pl.edu.pwsz.cinema.commerce.entity;

import lombok.Getter;
import lombok.Setter;
import pl.edu.pwsz.cinema.repertoire.entity.Showing;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private LocalDateTime creationDate;
    private String firstName;
    private String lastName;
    @Column(name = "is_vip")
    private boolean vip;

    @ManyToOne
    @JoinColumn(name = "showing_id")
    private Showing showing;
}
